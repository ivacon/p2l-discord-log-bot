import nextcord
import os
import time
from nextcord.ext import commands

tokenfile = open("token.txt", "r")
token = tokenfile.read()



bot = commands.Bot()
if os.path.exists("data") == False:
    os.system("mkdir data")

@bot.event
async def on_ready():
    game = nextcord.Game("with your logs.")
    await bot.change_presence(status=nextcord.Status.online, activity=game)

print("Init complete")

# - init code

@bot.slash_command(description="Ping")
async def ping(interaction: nextcord.Interaction):
    await interaction.send("Pong!")
    print(str(interaction.user.id) + " ran command /ping at " + str(time.time()))

@bot.slash_command(description="List items")
async def list(interaction: nextcord.Interaction):
    list = os.listdir('data')
    await interaction.send(list)
    print(str(interaction.user.id) + " ran command /list at " + str(time.time()))

@bot.slash_command(description="Create a new item")
async def create(interaction: nextcord.Interaction, name: str):
    if os.item.exists("data/ " + name):
        await interaction.send("ERR: Item already exists.")
        print(str(interaction.user.id) + " tried to create item " + name + " at " + str(time.time()) + " but it already existed.")
    else:
        create = open("data/" + name, "x")
        create.close()
        await interaction.send("Successfully created item: " + name)
        print(str(interaction.user.id) + " created item: " + name + " at " + str(time.time()))

@bot.slash_command(description="Add a new entry to an existing item")
async def addentry(interaction: nextcord.Interaction, item: str, entry: str):
    item = open("data/" + item, "a")
    item.write(entry + "\n")
    item.close()
    await interaction.send("Successfully added entry to item")
    print(str(interaction.user.id) + " added entry " + entry + " to item " + item + " at " + str(time.time()))

@bot.slash_command(description="List entries in item")
async def listentries(interaction: nextcord.Interaction, item: str):
    file = open("data/" + item, "r")
    await interaction.send(file.read())
    print(str(interaction.user.id) + " read entries of item " + item + " at " + str(time.time()))

@bot.slash_command(description="Get help information")
async def help(interaction: nextcord.Interaction):
    helpEmbed = nextcord.Embed(
        title = "Pay-to-lose server bot help",
    )
    helpEmbed.set_author(name = "Coded by /bin/Vuraniute#1517")
    helpEmbed.add_field(name = "``/help`` - Displays help information", value = "-")
    helpEmbed.add_field(name = "``/ping`` - Pong!", value = "-")
    helpEmbed.add_field(name = "``/list`` - Lists all Items", value = "-")
    helpEmbed.add_field(name = "``/create <name>`` - Creates a new item", value = "-")
    helpEmbed.add_field(name = "``/addentry <item> <entry>`` - Adds an entry to an item", value = "-")
    helpEmbed.add_field(name = "``/listentries <item>`` - Lists entries in an item", value = "-")
    await interaction.send(embed = helpEmbed)
    print(str(interaction.user.id) + " ran command /help at " + str(time.time()))

print("Commands defined")
print("Log ready.")

print("Logged in!")
print("Bot started.")
print("Log started.")
print("-- BEGIN LOG --")


bot.run(token)
print("-- END LOG --")
print("Logged out.")
