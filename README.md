# P2L Server bot
This is a simple logger bot for the pay to lose discord server.
It is published under the GPL 3 license.

## Setup
### Create a bot
1. Go to https://discord.com/developers and create a new app.
2. Turn that app into a bot.
3. In the OAuth2 tab, create a random redirect (e.g. https://example.com)
4. Go to the OAuth2 URL Generator
5. Create an invite link with the following scopes and Perms:
Scopes:
- bot
- applications.commands
- guilds
- guilds.join
Permissions:
- Administrator
6. Invite it to your server
### Clone this repo
1. Download git if not installed.
Windows: Download git for Windows
macOS: ``brew install git``, I guess? I don't use macs.
Linux: Download it with your package manager or use a package manager application. You know how.
2. Open a terminal (e.g. Windows Terminal, iTerm2, Konsole, GNOME Terminal)
3. Type ``git clone `` and then the URL of this repository.
4. Press enter
5. DON'T CLOSE THE TERMINAL, We'll use it later.
### Create a token.txt file
The bot reads the token from a private token.txt file. You need to create this using the token of your discord bot
- Go to https://discord.com/developers and select your app.
- In the bot area, click reset token.
- Enter your 2FA code or Password if required
- Copy your token
- Create a new file in this repository called token.txt
- Paste your token there
### Run it.
- Go back to your terminal.
- ``cd`` to this repository
- ``pip install nextcord``
- ``python main.py``
